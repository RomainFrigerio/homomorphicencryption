\documentclass[12pt,a4paper,twoside]{article}

\usepackage{cite}
\usepackage{url}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{a4wide} 
\usepackage{amsmath}
\usepackage{amssymb} 
\usepackage{graphicx}
\usepackage{color}
\usepackage{fancybox}
\usepackage{moreverb}
\usepackage[T1]{fontenc}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{lineno}

% \usepackage{hangcaption}
\usepackage{listings}
\usepackage[english]{babel}
%\usepackage[utf8]{inputenc}

\renewcommand{\thesection}{\Roman{section}} 
\renewcommand{\thesubsection}{\arabic{subsection}}

%\fbox{}
%\shadowbox{}
%\doublebox{}
%\ovalbox{}
%\Ovalbox{}
%\shabox{}


% --- Logo Atos ---
%\makebox[\textwidth][l]{
%\raisebox{-15pt}[0pt][0pt]{
%\hspace{2.5cm}
%\includegraphics[scale=0.1]{Images/logo_atos.eps}
%}
%}
\title{Mid-Report}
\author{Frigerio Romain}
\date{\today}

\pagestyle{headings}

\begin{document}

\maketitle
\newpage

\linenumbers

\section{Problems of the FHE Schemes}
\indent Several problems come with the current implementations of FHE schemes which are in fact SHE schemes. Here is the list of the main problems encountered with those schemes:
\begin{itemize}
\item \textbf{It only deals with integers which means no real numbers}. This aspect is inherent to the encryption scheme itself. The coefficients of the polynomials of the message space are the decomposition of integers in a given basis. 
\item \textbf{Comparisons are not possible}. This aspect is inherent to the idea of encryption itself. If an order relation was preserved then it would be possible to find any encrypted message by dichotomy. In other words, given a certain encrypted message one could find a lower bound and an upper bound to this number. Then one could refine these bounds to get an arbitrary precise approximation of the original message.
\item \textbf{Divisions are not possible}. The first reason has been exposed before: the scheme itself doesn't provide the users with real numbers. The euclidean division is not possible as well because it implies being able to compare numbers which is not possible.
\item \textbf{In the SHE schemes, the multiplicative depth is fixed}. Since FHE schemes are not practicable, the SHE schemes have been created under the condition that a multiplicative depth has to be fixed first. This implies a need for a good optimisation of the algorithms: counting the number of multiplication, be sure not to compute twice the same thing, etc. More multiplications implies systematically a slower algorithm.
\item \textbf{The algorithms implemented in the encrypted domain are slower than in the plain text domain}. The main reason has been exposed before: the time taken to multiply two encrypted numbers is significant. The other reason is that the algorithm does not deal with numbers any more, it deals with \textbf{polynomials} instead which means slower operations and more memory space involved.
\item \textbf{The efficiency is dependent of the implementation of the used library}. The main idea here is that the efficiency of encrypted algorithm is \textbf{directly} correlated with how optimized is the library. The encrypted algorithms are only the transcription of plain text algorithms which means that it keeps the same multiplicative complexity. If the plain text algorithm is in $n^2$ in term of multiplication, the encrypted algorithm will be in $n^2$ too. However, the time taken to perform a multiplication is different. Thus, the efficiency is based on the optimisation of the library and on the machine on which the algorithm is run.
\end{itemize}

\newpage

\section{Solutions Explored}

There are two different approaches now that the main problems are well defined. The first is about finding a way to overcome the constraints brought by the scheme and then being able to implement all machine learning algorithms. The second is about finding specific machine learning algorithms which run within these constraints.\\


\noindent The main constraints to the usual algorithms are the inability to deal with \textbf{real numbers}, the inability to \textbf{compare} numbers and the inability to perform arbitrary \textbf{large number of multiplications}. Here are the different solutions explored to overcome these constraints:
\begin{itemize}
\item \textbf{Real numbers}. The idea here is to exploit the structure of the lattice used by the scheme \cite{export:258435}. All polynomial messages are considered modulo $X^n + 1$ which simply means that every $X^n$ is replaced by $-1$. Then let's denote a floating number $y$ by $p \mathbf{.} q$~. For instance: in $12.55$, $p = 12$ and $q = 55$. Then, let's denote $b_{\alpha^{+}}b_{\alpha^{+}-1} \cdots b_{1}b_{0}$ the binary decomposition of $p$ and $b_{-1}b_{-2} \cdots b_{-\alpha^{-}}$ the binary decomposition of $0 \mathbf{. }q$ such that:
$$
	y = \sum_{i \leq \alpha^{+}} b_iX^{i} - \sum_{0 < i \leq \alpha^{-}} b_{-i}X^{n-i}
$$
For instance, with this encoding system, $2$ is encoded as $X$ and $\frac{1}{2}$ is encoded as $-X^{n-1}$ because $\frac{1}{2} = 0.5 =\mathbf{1} \times 2^{-1}$. Then:
$$
	2 \times \frac{1}{2} = X \times -X^{n-1} = -X^n = 1 ~(\text{mod}~ X^n +1) 
$$
The result is then $\mathbf{1}$ as expected.\\
Let's take a less trivial example. Let's multiply $3.25 = 11.01_2$ encoded as $X + 1 - X^{n-2}$ and $1.5 = 1.1_2$ encoded as $1 - X^{n-1}$. Then the multiplication gives:
\begin{eqnarray*}
(X + 1 - X^{n-2})(1 - X^{n-1}) &=& X^{2n-3} - X^{n-2} - X^n - X^{n-1} + X + 1 \\
&=& -X^{n-1} - X^{n-2} - X^{n-3} + \underbrace{(X + 2)}_{X^2}~ (\text{mod}~ X^n + 1)
\end{eqnarray*}
Then decoding gives $2^2 + 2^{-1} + 2^{-2} + 2^{-3} = \mathbf{4.875}$ as expected.\\~\\
This encoding system comes with a new constraint however. In the SHE scheme, the degree of the polynomial modulus $n$ is \textbf{fixed} before any encryption. Indirectly, $n$ represents the number of bits (coefficients) available to encode a signed integer if the base $2$ is chosen. To use the encoding presented above, it is necessary to define $\alpha^{+}$ (respectively $\alpha^{-}$) which is roughly the maximum number of bits dedicated for $p$ (respectively $q$). Then the relation $\alpha^{+} + \alpha^{-} \leq n$ holds. Thus, the first new constraint is that two new parameters have to be fixed at the beginning.\newpage The second limitation of the method is that this encoding system brings a new \textit{"indirect noise"}. Let $y$ be encoded with this system and let $k$ be the number of coefficients necessary to encode the left part of $y$ and let $l$ be the number of coefficients necessary to encode the right part. Then $k$ is upper bounded by $\alpha^{+}$ and $l$ is upper bounded by $\alpha^{-}$. So with each multiplication $k$ and $l$ increase. Then, if one of them crosses its respective threshold $\alpha$, the result will not be decryptable any more.
 
\item \textbf{Comparison}. One way to overcome the limitations associated with the comparison of encrypted values is to add a \textbf{third trusted party} to the encrypted analysis scheme. In a one-to-one system, being able to compare encrypted numbers implies a way to recover any message. However, by adding a third trusted party in the system (trusted by the owner of the data) it becomes possible to \textbf{monitor} precisely what is compared in the system and then regulate these operations. Concretely there are two main cases:
\begin{itemize}
\item The third party is \textbf{fully} trusted. In this case, the \emph{owner} gives his \emph{private key} to him. The user sends requests for comparisons and the third party can choose to answer or not. Note that the third party can also be the owner of the data. The answer of the third party can be a boolean (\textit{true} or \textit{false}) or an \textbf{encrypted} $1$ or $0$. The second option is of interest because the user \textbf{has never access to the raw result of the comparison}. This cannot be applied to all machine learning algorithms but it is particularly suited to simple machine learning algorithms like the Perceptron algorithm for instance. The reason is that for this algorithm, \textbf{the result of the core comparison leads to simple updates}. Concretely:

\begin{algorithmic}
\IF{$(y(w_0 + <w,x>) \leq 0)$}
\STATE $w_0 = w_0 + \eta \times y$
\STATE $w = w + \eta \times y \times x$
\ENDIF
\end{algorithmic}

Becomes
\begin{algorithmic}
\STATE $\text{comp} = \text{comparison\_protocol\_leq}(y(w_0 ~+ <w,x>),~0$)
\STATE $w_0 = \text{comp} \times (w_0 + \eta \times y) + (1 - \text{comp}) \times w_0$
\STATE $w = \text{comp} \times (w + \eta \times y \times x) + (1 - \text{comp}) \times w$ 
\end{algorithmic}
Which might even be optimised:
\begin{algorithmic}
\STATE $\text{comp} = \text{comparison\_protocol\_leq}(y(w_0 ~+ <w,x>),~0$)
\STATE $w_0 = w_0 + \text{comp} \times (\eta \times y)$
\STATE $w = w + \text{comp} \times (\eta \times y \times x)$ 
\end{algorithmic}
\vspace{2mm}
\item The third party is \textbf{semi-trusted}. In this case, the owner also gives his \textit{private key} to the third party but doesn't want him to decrypt the raw data which can be sent by the user within the comparison requests. In fact, this case is only slightly different from the other. The global ideas remain the same but here the idea will be to \textbf{hide the comparisons from all the players}. Concretely, instead of using the above protocol to compare encrypted $a$ and $b$, the user will use the protocol to compare encrypted $a + r$ and $b + r$ with $r$ a random encrypted number drawn at each comparison request. This simple operation then creates a complete secured system where nobody except the owner of the data gets access to it. \\From the user perspective, since everything is encrypted, it is not possible to recover anything. From the third party perspective, all the numbers sent are \textbf{uniformly sampled} in a given interval, according to the information theory, they contain no information. Then it is not possible to know what exactly is compared but it is still possible to give an accurate (encrypted) answer.
\end{itemize}

\item \textbf{Limitation on the number of multiplications}. For this problem, there are roughly two main approaches.
\begin{itemize}
\item The first one is to thoroughly look at the algorithm and determine how many multiplications will be performed. Then one can develop a system that will only perform upto the maximum number of multiplications. The drawback of this approach is that the more multiplications are necessary the slower the execution becomes. This is an inherent drawback of the SHE schemes.

\item The second option is to find a way to refresh the encryption after a fixed number of multiplication, a kind of reset. This approach is better when loops have to be executed. For instance, a loop of $2$ multiplications executed $100$ times will need $200$ multiplications. A SHE scheme allowing $200$ multiplications is impracticable. However, setting the refresh every $10$ multiplications will change the loop into $20$ loops of $5 \times 2$ multiplications plus one communication for the refresh. In fact, the refresh protocol is based on the same idea as the one used in the comparison protocol. The user sends the encrypted number to the third party. It can be as is or blinded with a random number $r$. Then the third party sends back the encryption of the decryption. Thus, the "encrypted number is reset".
\end{itemize}

\item \textbf{Computation constraints}. To overcome this problem, there are plenty of little things to be aware of. First, always try to optimise the algorithms because a single operation costs a lot more in the encrypted domain than in the plain text domain. Then the user should choose an implementation of the algorithm that has been optimised for the required task and the resources available in terms of memory and computing power. For instance, the R library \emph{HomomorphicEncryption} is easy to use but doesn't allow the multiplication with a plain text number which is much more efficient than usual multiplication. The SEAL library allows this operation but is not meant to be as easy to use.
\end{itemize}

\newpage
\section{Algorithms}

By understanding and exploring ways to overcome all the limitations presented in the previous section, a machine learning algorithm can be developed to analyse encrypted data. Three different algorithms have been explored. Each one is meant to be a showcase of a few solutions explored before. A git repository containing all the scripts is available here \cite{git}. 

\subsection*{The Dataset}

The algorithms have been applied to a real data base related to \textbf{health} and publicly available here \cite{disease}. This is a \textbf{classification} problem and all the algorithms are built to determine the boundary between the presence or the absence of a heart disease.  The dataset consists of a design matrix of \textbf{$297$ observations and $14$ variables} with the last one which is the predicted attribute from $0$ to $4$. The R library does not allow the use of real numbers so the columns containing floats have been \textbf{scaled by a power of $10$ and then rounded}. \\The description of the data available on the website says: \emph{"Experiments with the Cleveland database have concentrated on simply attempting to distinguish presence (values 1,2,3,4) from absence (value 0)"}. Thus, the idea will be to build algorithms of the type \emph{"classification of 0 versus all"}.

\subsection{Perceptron}

This algorithm is the kind of basis of machine learning and that is why we wanted to try this one first. It allowed us to experiment the \textbf{comparison and refreshing protocols} on a real case example. The protocols are supposed to be executed by a third party but it is simulated as a function in all the different scripts. Let inspect the different part of the encrypted Perceptron script.
~\\
\begin{itemize}
\item \textbf{Comparison protocol}
\begin{lstlisting}[language=R]
# Returns an encrypted '1' if a <= b and an encrypted '0' otherwise
# 'a' and 'b' are blinded number: a = x + r and b = y + r
comparison <- function(a, b, sk, pk) {
  if (dec(sk, a) <= dec(sk,b)) {
    return (enc(pk, 1))
  } else {
    return (enc(pk, 0))
  }
}
\end{lstlisting}

\newpage

\item \textbf{Refreshing protocol}
\begin{lstlisting}[language=R]
# Returns the same number encrypted so that the algorithm 
# can perform multiplications again
# 'x' is a blinded number: x = a + r
refresh <- function(x, sk, pk) {
  return (enc(pk, dec(sk,x)))
}
\end{lstlisting}
\end{itemize}

In these two protocols, the argument \emph{sk} represents the \emph{secret key} possessed by the third party. The arguments \emph{'a'}, \emph{'b'} and \emph{'x'} represent the messages received by the third party. The \emph{'returns'} represent what the third party sends back to the user.\\
The main function of the Perceptron script is available in the Appendix \ref{perceptron}. It remains the same algorithm as in clear but translated into a script which runs in the encrypted domain. It consists in $10$ loops of $5$ updates of the normal vector $\vec{\omega}$. The script has been run $10$ times and gives an average accuracy of $\mathbf{59,2\%}$. Note that in our case, the learning rate $\eta$ is always equal to $1$ because it is not possible to deal with real numbers.

\subsection{Adaline}
The two interesting things to test with this algorithm are the fact that \textbf{it does not need any comparison} and the fact that it can be seen as one of the simplest \emph{"gradient descent based learning algorithm"}. Those algorithms are very common in machine learning and being able to implement them in the encrypted domain could allow us to solve a lot of different problems.\\
Concretely, the algorithm in the encrypted domain consists simply in getting rid of the comparison and changing the update rule of the Perceptron algorithm \ref{perceptron} to:
\begin{lstlisting}[language=R]
sp <- w %*% x.curr + w0
w0 <- w0 + eta * (y.curr - sp)
w <- w + eta * (y.curr - sp) * x.curr
\end{lstlisting}
This algorithm gives good results in the \textbf{clear domain} because it is possible to tune the learning rate $\eta$ as accurately as necessary especially by setting it to a float. However, for the moment and with the R library, it is not possible to set a floating $\eta$. The immediate implication of this fact is that, after a few loops, the components of the vector $\vec{\omega}$ are too big and then the algorithm doesn't converge in the encrypted domain. 

\newpage

\subsection{Estimated Logistic Regression}
~\\
This algorithm has a huge potential because it is \textbf{deterministic}, \textbf{it does not need any comparison} and \textbf{it does not need any refreshing}. It is mainly inspired by the work referenced here \cite{aslett2015encrypted}.

\subsubsection{Framework of the Algorithm}
\noindent There are two different labels denoted $y$: $\mathbf{1}$ for the \emph{absence of the disease} and $\mathbf{0}$ for the \emph{presence of the disease}. Let denote $\mathcal{D}$ the set of data available for the classification\\ $\{x_j~;~j=1\cdots n\}$. Then the Naive Bayes Framework used for this algorithm says according to the Bayes Theorem:
\begin{equation*}
	\mathbb{P}(y|\mathcal{D}) \propto \mathbb{P}(\mathcal{D}|y)\mathbb{P}(y)
\end{equation*}
And using an \emph{independence assumption}:
\begin{equation}
	\mathbb{P}(\mathcal{D}|y) = \prod_{j=1}^{n}\mathbb{P}(x_j|y)
\end{equation}
Then it is possible to write the basis of the framework:
\begin{equation}
\mathbb{P}(y=1|\mathcal{D}) = \frac{\mathbb{P}(\mathcal{D}|y=1)\mathbb{P}(y=1)}{\mathbb{P}(\mathcal{D}|y=1)\mathbb{P}(y=1) + \mathbb{P}(\mathcal{D}|y=0)\mathbb{P}(y=0)}
\end{equation}
Then by using the equation (1) and after some computations, it is possible to show:
\begin{equation}
\log\left( \frac{\mathbb{P}(y=1|\mathcal{D})}{\mathbb{P}(y=0|\mathcal{D})} \right) = (n-1)\log\left( \frac{\mathbb{P}(y=1)}{\mathbb{P}(y=0)} \right) + \sum_{j=1}^{n} \log\left( \frac{\mathbb{P}(y=1|x_j)}{\mathbb{P}(y=0|x_j)} \right)
\end{equation}
Then the idea is to assume a \emph{linear decision boundary} which leads to:
\begin{equation*}
\log\left( \frac{\mathbb{P}(y=1|x_j)}{\mathbb{P}(y=0|x_j)} \right) = \alpha_j + \beta_j x_j ~~~~~~ \text{and} ~~~~~~ \frac{\mathbb{P}(y=1)}{\mathbb{P}(y=0)} = \theta
\end{equation*}
Then the goal of this algorithm will be to find \emph{the optimized set of parameters} $\{\hat{\alpha}, \hat{\beta}, \hat{\theta}\}$. The parameter $\theta$ can be optimized independently of the others:
\begin{equation*}
	\hat{\theta} = \frac{\sum_{j=1}^{n} y_j}{n - \sum_{j=1}^{n} y_j}
\end{equation*}
The optimisation of $\alpha$ and $\beta$ will be described in the next section.\\
The final step to build the framework of the algorithm is to write the estimated equation (3) into:
\begin{equation*}
	\hat{\Phi}(x_j;\hat{\alpha},\hat{\beta},\hat{\theta}) =  \log\left( \frac{\mathbb{P}(y=1|x_j;\hat{\alpha},\hat{\beta},\hat{\theta})}{\mathbb{P}(y=0|x_j;\hat{\alpha},\hat{\beta},\hat{\theta})} \right) = (n-1)\log(\hat{\theta}) + \sum_{j=1}^{n} \hat{\alpha}_j + \hat{\beta}_j x_j
\end{equation*} 
And finally, the equation (2) into:

\begin{equation}
\mathbb{P}(y=1|\mathcal{D}) = \frac{\displaystyle 1}{\displaystyle 1 + \frac{\displaystyle \mathbb{P}(\mathcal{D}|y=0)\mathbb{P}(y=0)}{\displaystyle \mathbb{P}(\mathcal{D}|y=1)\mathbb{P}(y=1)}} = \frac{\displaystyle 1}{\displaystyle 1 + \exp \left(-\hat{\Phi}(x_j;\hat{\alpha},\hat{\beta},\hat{\theta}) \right)}
\end{equation}
~\\~\\ \noindent Here appears the logistic function $\sigma(x) = \frac{\displaystyle 1}{\displaystyle 1 + \exp(-x)}$ which is at the core of the \emph{logistic regression}.

\subsubsection{Optimisation of $\alpha$ and $\beta$}

There exists an \emph{iterative} algorithm giving the computation of the optimized parameters $\alpha$ and $\beta$ called Iteratively Reweighted Least Square (IRLS) algorithm. As we are going to see in the next paragraph, it is not possible to run this algorithm entirely in the encrypted domain because it involves the \emph{computation of non-linear functions to update the weights}. The idea will be to \emph{only use the first step of the algorithm starting from a uniformly sampled initial guess}.\\~\\

\noindent The iterations of this algorithm are obtained by using the Newton-Raphson iterations. This is a well-known algorithm used to find the approximation of the zeros of a function (which could sometimes be non-linear). Starting from an initial guess $\beta^{(0)}$, the update is done as follows:
\begin{eqnarray*}
	\eta_i^{(0)} &=& X_{i\bullet} \beta^{(0)} \\
	\mu_i^{(0)} &=& \sigma(\eta_i^{(0)}) \\
	\omega_{ii}^{(0)} &=& \mu_i^{(0)}(1 - \mu_i^{(0)}) \\
	z_i^{(0)} &=& \eta_i^{(0)} + (\omega_{ii}^{(0)})^{-1}(y_i - \mu_i^{(0)})
\end{eqnarray*}
And then the final update:
\begin{equation}
	\beta^{(1)} = \left( X^T W^{(0)} X \right)^{-1} X^T W^{(0)}z^{(0)}
\end{equation}
Where $X_{i\bullet}$ denotes the $i$th row of $X$ and $w_{ii}^{(0)}$ the weight for the $i$th variable at the initial step. Note that the design matrix $X$ is in its general form with a first column of $1$'s for the intercept. The idea to make it work in the encrypted domain is to choose an \emph{appropriate initial guess} for $\beta^{(0)}$ which gives a close form for the weights. \\~\\The easiest one is $\beta^{(0)} = (0,\cdots,0)^T$ and will be the one used from now. \\Thus, $\forall i~~ \eta_i^{(0)} = 0,~~ \mu_i^{(0)} = \frac{1}{2},~~ \omega_{ii}^{(0)} = \frac{1}{4}$ and $z_i^{(0)} = 4y_i - 2$. \\Then, the first step of the IRLS (5) leads to:
\begin{equation*}
\{\hat{\alpha}_j, \hat{\beta}_j\} = \{a_j/d_j, b_j/d_j\}
\end{equation*}
Where:
\begin{eqnarray*}
	a_j &=& \left( \sum_{i=1}^{n} x_{ij}^2 \right) \left( \sum_{i=1}^{n} z_i^{(0)} \right) - \left( \sum_{i=1}^{n} x_{ij} \right) \left( \sum_{i=1}^{n} x_{ij}z_i^{(0)} \right) \\
	b_j &=& N \sum_{i=1}^{n} x_{ij}z_i^{(0)} - \left( \sum_{i=1}^{n} x_{ij} \right) \left( \sum_{i=1}^{n} z_i^{(0)} \right) \\
	d_j &=& N \sum_{i=1}^{n} x_{ij}^2 - \left( \sum_{i=1}^{n} x_{ij} \right)^2
\end{eqnarray*}
Now that all the parameters are estimated, making a prediction for a new observation $x^{*}$ is simply performed by using the equation (4):
\begin{equation*}
	\boxed{\mathbb{P}(y=1|x^{*};\hat{\alpha}, \hat{\beta}, \hat{\theta}) = \sigma\left( \hat{\Phi}(x^{*};\hat{\alpha},\hat{\beta},\hat{\theta}) \right)}
\end{equation*}
~\\
\subsubsection{Algorithm in the Encrypted Domain}
As the reader may have noticed, the computation of all the parameters involve the computation of \emph{ratios}. However, since it is \emph{not possible} to do any division in the encrypted domain, the idea is to compute separately the numerators and denominators of each estimation. Then, back in the plain text domain, it would be possible to \emph{"assemble the bits together"} to get the final estimated parameters. The R translation of what has been said before is available here \ref{logreg}. \\The data set has been separated into a training set of randomly chosen $200$ observations called (train, Y.train) and a test set called (test, Y.test). The whole script is available in the Git repository \cite{git} in the name of \emph{"bayes\_disease\_HE.R"}.\\
Running the script on the previously presented data set \emph{Cleveland Heart Disease}, the result is $\mathbf{82,5\%}$ classification accuracy on an average of $10$ tests.
\newpage

\bibliographystyle{unsrt}
\bibliography{biblio}

\newpage

\section{Appendix}
\subsection*{Perceptron}
\label{perceptron}
\begin{lstlisting}[language=R]
# Performs the perceptron algorithm on encrypted data.
perceptron.enc <- function(X, y, sk, pk) {
  # Initial variables
  nloop <- 10
  nit <- 5
  n <- dim(X)[1]
  m <- dim(X)[2]
  
  # '1' and '0' encrypted for the comparison process
  zero <- enc(pk, 0)
  one <- enc(pk, 1)
  security.param <- 60 # The blinding number will be uniformly sampled 
                       # in [1,security.param] 
  
  # Initialisation of the algorithm. 
  # /!\ Everything is computed in the encrypted domain /!\
  w0 <- zero
  w <- enc(pk, rep(0, m))
  
  # Main loop of the perceptron
  for (i in 1:nloop) {
    for (j in 1:nit) {
      r <- floor(runif(1) * n) + 1 # Choose a random observation
      y.curr <- y[r]
      x.curr <- X[r,]
      
      blind <- enc(pk, floor(security.param * runif(1)))
      comp <- comparison(y.curr * (w %*% x.curr) + w0 + blind, 
                         zero + blind, sk, pk)
      # The 'if' part of the perceptron linearized
      w0 <- comp * ( w0 + y.curr ) + (one - comp) * w0
      w <- comp * (w + y.curr * x.curr) + (one - comp) * w
    }  
    # Refresh the cyphertext
    w0 <- refresh(w0, sk, pk)
    w <- refresh(w, sk, pk)
  }
  return (dec(sk, c(w0, w)))
}
\end{lstlisting}

\subsection*{Estimated Logistic Regression}
\label{logreg}

\begin{lstlisting}[language=R]
# Main function estimating the parameters of the plane (alpha, beta) 
# such that the equation of the plane is alpha + beta * x. 
# It divides the variable space into two parts and gives
# a way to classify a new observation x0.
bayes <- function(X, y, pk) {
  n = length(y)
  p = dim(X)[2]
  a = enc(pk, rep(0, p))
  b = enc(pk, rep(0, p))
  d = enc(pk, rep(0, p))
  
  z = enc(pk, 4) * y - enc(pk, 2)
  n.enc = enc(pk, n)
  sum.z = sum(z)
  theta.num = sum(y)
  
  for (j in 1:p) {
    sum.x2 = sum(X[,j] * X[,j])
    sum.x = sum(X[,j])
    ps.xz = X[,j] %*% z
    
    a[j] = sum.x2 * sum.z - sum.x * ps.xz
    b[j] = n.enc * ps.xz - sum.x * sum.z
    d[j] = n.enc * sum.x2 - sum.x * sum.x
  }
  
  return (list(a = a, b = b, d = d, theta.num = theta.num))
}

(...)

# Computing of the parameters alpha and beta.
# It is performed by the trusted third party with the private key sk
a = dec(k$sk, result$a)
b = dec(k$sk, result$b)
d = dec(k$sk, result$d)
theta.num = dec(k$sk, result$theta.num)

alpha = a/d
beta = b/d
theta = theta.num/(length(Y.train) - theta.num)
\end{lstlisting}

\end{document}
