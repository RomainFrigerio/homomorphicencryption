library("HomomorphicEncryption")

##############################################################################################
#################################### TRUSTED THIRD PARTY #####################################
##############################################################################################

# Comparison protocol. 
# Return an encrypted '1' if a <= b and an encrypted '0' otherwise
# 'a' and 'b' are blinded number: a = x + r and b = y + r
comparison <- function(a, b, sk, pk) {
  if (dec(sk, a) <= dec(sk,b)) {
    return (enc(pk, 1))
  } else {
    return (enc(pk, 0))
  }
}


# Refresh protocol.
# Returns the same number reincripted so that the algorithm can perfoms multiplications again
# 'x' is a blinded number: x = a + r
refresh <- function(x, sk, pk) {
  return (enc(pk, dec(sk,x)))
}



##############################################################################################
######################################### CLOUD ############################################## 
##############################################################################################


# Performs the perceptron algorithm on encrypted data.
# /!\ The depth of the encryption scheme is closely related to 't' /!\
perceptron.enc <- function(X, y, sk, pk) {
  # Initial variables
  nloop <- 2
  nit <- 5
  n <- dim(X)[1]
  m <- dim(X)[2]
  
  # '1' and '0' encrypted for the comparison process
  zero <- enc(pk, 0)
  one <- enc(pk, 1)
  security.param <- 60 # The blinding number will be uniformly sampled in [1,security.param] 
  
  # Initialisation of the algorithm. /!\ Everything is done in the encrypted domain /!\
  w0 <- zero
  w <- enc(pk, rep(0, m))
  
  # Main loop of the perceptron
  for (i in 1:nloop) {
    for (j in 1:nit) {
      r <- floor(runif(1) * n) + 1 # Choose a random observation
      y.curr <- y[r]
      x.curr <- X[r,]
      
      blind <- enc(pk, floor(security.param * runif(1)))
      comp <- comparison(y.curr * (w %*% x.curr) + w0 + blind, zero + blind, sk, pk) # The protocol returns '1' or '0' encrypted
      
      # The 'if' part of the perceptron linearized
      w0 <- comp * ( w0 + y.curr ) + (one - comp) * w0
      w <- comp * (w + y.curr * x.curr) + (one - comp) * w
    }
    
    # Refresh the cyphertext
    w0 <- refresh(w0, sk, pk)
    w <- refresh(w, sk, pk)
  }
  
  return (c(w0, w))
}


# Performs the exact same algorithm in the plaintext domain.
# Aim: Compare the two results
perceptron.clear <- function(X, y) {
  t <- 10
  n <- dim(X)[1]
  m <- dim(X)[2]
  
  w0 <- 0
  w <- rep(0, m)
  
  for (nit in 1:t) {
    # r <- ((nit-1) %/% n) + 1 
    r <- floor(runif(1) * n) + 1
    y.curr <- y[r]
    x.curr <- X[r,]
    
    if (y.curr * ( (w %*% x.curr) + w0) <= 0) {
      comp <- 1
    } else {
      comp <- 0
    }
    
    w0 <- comp * ( w0 + y.curr ) + (1 - comp) * w0
    w <- comp * (w + y.curr * x.curr) + (1 - comp) * w
  }
  
  return (c(w0, w))
}


# Plots the differents points along with their label.
# Plots the line given by the perceptron.
plot.result <- function(X, y, w) {
  plot(-5:5, -5:5, type = "n")
  for (i in 1:dim(X)[1]) {
    if (y[i] == 1) {
      points(X[i,1], X[i,2], col="red", pch=16)
    } else {
      points(X[i,1], X[i,2], col="blue", pch=16)
    }
  }
  abline(a = -w[1]/w[3], b = -w[2]/w[3])
}

predict.perceptron.unit <- function(x, w) {
  n <- length(x)
  sp <- w[1] + w[2:(n+1)] %*% as.numeric(x)
  
  if ( sp <= 0 ) {
    return (-1)
  } else {
    return (1)
  }
}

predict.perceptron <- function(X, w) {
  n <- dim(X)[1]
  Y <- c()
  
  for (i in 1:n) {
    Y[i] <- predict.perceptron.unit(X[i,], w)
  }
  
  return (Y)
}


# Initial step: generating pair of keys
p <- parsHelp("FandV", lambda = 80, max = 100, L = 12)
k <- keygen(p)

# Generation of examples
X <- matrix(c(1,2,3,4,-3,-2,4,2 , 2,3,2,1,1,3,5,0), nrow = 8, ncol = 2)
y <- c(-1, -1, 1, 1,-1,-1,-1,1)
X.enc <- enc(k$pk, X)
y.enc <- enc(k$pk, y)

# Performing perceptron in the two domains
w <- perceptron.enc(X.enc, y.enc, k$sk, k$pk)
w.clear <- perceptron.clear(X,y)
w.dec <- dec(k$sk, w)

# Plot the results
plot.result(X, y, w.dec)

# Test if well classified
Y.try <- predict.perceptron(X,w.dec)
acc <- sum((Y.try == y))/length(y)