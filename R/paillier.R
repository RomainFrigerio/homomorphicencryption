require("homomorpheR")

# Generate a pair of keys (pub, priv) and the functions to encrypt/decrypt
keys <- PaillierKeyPair$new(1024)
encrypt <- function(x) keys$pubkey$encrypt(x)
decrypt <- function(x) keys$getPrivateKey()$decrypt(x)

# Test on a single big integer
a <- gmp::as.bigz(1273849)
identical(a + 10L, decrypt(encrypt(a+10L)))

# Test of the homomorphic part (addition) of the Paillier encryption
x <- random.bigz(nBits = 512)
y <- random.bigz(nBits = 512)
xy.plus <- x + y
xy.mult <- x*y

x.encrypt <- encrypt(x)
y.encrypt <- encrypt(y)
identical(xy.plus, decrypt(keys$pubkey$add(x.encrypt, y.encrypt)))
identical(xy.plus, decrypt(x.encrypt * y.encrypt)) # Same as above

# Trying to take the log
x.log.encrypt <- encrypt(log(x))
y.log.encrypt <- encrypt(log(y))
identical(xy.mult, exp(decrypt(keys$pubkey$add(x.log.encrypt, y.log.encrypt))))

# Doesn't work because Paillier is a partial homomorphic system for the addition
# identical(xy.mult, decrypt(keys$pubkey$mult(x.encrypt, y.encrypt)))